#include<iostream>
using namespace std;

int main(){
        int t;
        cin >> t;
        while(t--){
		int n1,n2;
		cin >> n1 >> n2;
		string s1,s2;
		cin >> s1 >> s2;
		int dp[n1+1][n2+1];
		for(int i=0;i<=n1;i++){
			for(int j=0;j<=n2;j++){
				if(i==0 || j ==0)
					dp[i][j] = max(i,j);
				else{
					if(s1[i-1]==s2[j-1])
						dp[i][j] = dp[i-1][j-1];
					else
						//int a = min(1,2,3);
						dp[i][j] = 1+ min(dp[i-1][j], min(dp[i][j-1],dp[i-1][j-1]));
				}
			}
		}
		cout << dp[n1][n2] << endl;
        }
        return 0;
}
