#include<iostream>
using namespace std;

int cal(int start,string s,int arr[]){
	if(s[start] == '0')
		return 0;
	if(start >= s.length()-1){
		arr[start] = 1;
		return 1;

	}

	int i = stoi(s.substr(start,2));
	if(i>=1 && i<=26){
		arr[start] = cal(start +1,s,arr) + cal (start +2,s,arr);
		return arr[start];
	}
	else{
		arr[start] = cal(start +1,s,arr);
		return arr[start];
	}
	return 1;
}

int main(){
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		string s;
		cin >> s;
		int arr[n] = {0};
		cal(0,s,arr);
		cout << arr[0] << endl;
	}
	return 0;
}
