#include<vector>
#include<iostream>
#include<deque>
#define forall(i,a,b) for(int i=a;i<b;i++)
using namespace std;

void maxElem(int n,int k,int arr[],vector<int> &ans){
	deque<int> dq;
	forall(i,0,k){
		while(!dq.empty() && arr[i]>=arr[dq.back()])
			dq.pop_back();
		dq.push_back(i);
	}
	forall(i,k,n){
//		cout << "pushback " << arr[dq.front()] << endl;
		ans.push_back(arr[dq.front()]);
		if(dq.front() <= i-k)
			dq.pop_front();
		while(!dq.empty() && arr[i]>=arr[dq.back()]){
			dq.pop_back();
		}
//			cout << arr[i] << " "  << dq.size();
//			if(!dq.empty())
//				cout << " " << dq.front() << " " << dq.back();
//			cout << endl;
		dq.push_back(i);
	}
//		cout << "pushback " << arr[dq.front()] << endl;
	ans.push_back(arr[dq.front()]);
//	ans.push_back(90);
//	cout << " size " << ans.size() << endl;
}

int main(){
        int t;
        cin >> t;
        while(t--){
		int n,k;
		cin >> n >> k;
		int arr[n];
		forall(i,0,n)
			cin >> arr[i];
		vector<int> ans;
		maxElem(n,k,arr,ans);
		forall(i,0,ans.size())
			cout << ans[i] << " ";
		cout << endl;
        }
        return 0;
}
