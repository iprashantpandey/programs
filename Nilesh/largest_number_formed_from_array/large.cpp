#include<vector>
#include<algorithm>
#include<iostream>
using namespace std; 

bool compare(string s1,string s2){
	return (stoi(s1+s2) > stoi (s2+s1));
}

int main(){
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		vector<string> s(n,"");
		for(int i=0;i<n;i++)
			cin >> s[i];
		sort(s.begin() , s.end(), compare);
		for(int i=0;i<n;i++)
			cout << s[i];
		cout << endl;
	}
	return 0;
}
