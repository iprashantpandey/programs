#include<iostream>
#include<queue>
using namespace std;

int main(){
        int t;
        cin >> t;
        while(t--){
		int n,k;
		cin >> n >> k;
		vector<int> vec;
		for(int i=0;i<n;i++){
			int a;
			cin >> a;
			vec.push_back(a);
		}
		priority_queue<int> pq(less<int>() , vec);
		for(int i=0;i<k;i++){
			cout << pq.top() << " ";
			pq.pop();
		}
		cout << endl;
        }
        return 0;
}
