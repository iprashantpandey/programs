#include<iostream>
using namespace std;

void convertToRoman(int n)
{
    string s = "";
    int a = n/1000;
    while(a--){
        s += 'M';
    }
    n = n % 1000;
    if( n >=500)
        s+= 'D';
    n = n % 500;
    a = n/100;
    while(a--){
        s += 'C';
    }
    n = n%100;
    if(n >= 50)
        s += 'L';
    n = n % 50;
    a = n /10;
    while(a--)
        s+= 'X';
    n = n %10;
    if(n >= 5)
        s+='V';
    n = n%5;
    while(n--){
        s+='I';
    }
    cout <<s ;

}

int main(){
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		convertToRoman(n);
		cout << endl;
	}
}
