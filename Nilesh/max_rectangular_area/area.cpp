#include<iostream>
#include<stack>
using namespace std;

struct Node{
	int start,value;
	Node(int s,int v){
		start = s;
		value = v;
	};
};

int maxVal;

void finalCal(stack<Node> &s,int n){
	while(!s.empty()){
		Node a = s.top();
		s.pop();
		int area = a.value * (n - a.start);
		if(area > maxVal)
			maxVal = area;
	}
}

void cal(int start,int value,int i){
	int area = (i-start) * value;
	if(maxVal < area)
	 	maxVal = area;
}

void pushStack(int i, int value, stack<Node> &s){
	Node *a = new Node(i,value);
	s.push(*a);
//	cout  << i << "inside push "  << (s.top()).value << endl;
}

void popStack(int i, int value, stack<Node> &s){
//	cout  << i << "inside pop "  << (s.top()).value << endl;
	int last = 0;
	while(!s.empty()){
		Node a = s.top();
		if(a.value == value)
			return;
		s.pop();
		if(a.value > value){
			last = a.start;
			cal(a.start,a.value,i);
		}
		else if(a.value < value){
			s.push(*(new Node(last,value)));
		}
	}
	if(s.empty())
		s.push(*(new Node(last,value)));
}

int maxArea(int n,int arr[],stack<Node> &s){
	int last = 0;
	for(int i=0;i<n;i++){
//		cout << i << " val " << arr[i]<< endl;
		if(last == arr[i])
			continue;
		if(last > arr[i])
			popStack(i,arr[i],s);
		else if(last < arr[i])
			pushStack(i,arr[i],s);
		last = arr[i];
//		cout << " max  " << maxVal << endl;
	}
	finalCal(s,n);
	return maxVal;
}

int main(){
        int t;
        cin >> t;
        while(t--){
		stack<Node> s;
		maxVal = 0;
		int n;
		cin >> n;
		int arr[n];
		for(int i=0;i<n;i++)
			cin >> arr[i];
		cout << maxArea(n,arr,s) << endl;
        }
        return 0;
}
