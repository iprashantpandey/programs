#include<iostream>
using namespace std;
struct Node
{
    int data;
    Node* left;
    Node* right;
};
int calc(Node *node,int current, int *minValue){
	if(node == NULL)
		return 0;
	if(*minValue >= current)
		return 1;
	int left = 1+ (calc(node->left,current+1,minValue));
	int right =1+ (calc(node->right,current+1,minValue));
	return min(left,right);
	
}
int minDepth(Node *node){
 	int current = 1;
	int minValue = INT_MAX;
	if(node == NULL)
		return 0;
	int left = 1+ (calc(node->left,current+1,&minValue));
	int right =1+ (calc(node->right,current+1,&minValue));
	return min(left,right);
}
