#include<iostream>
using namespace std;

struct Node{
	char data;
	struct Node *left;
	struct Node *right;

	Node(char x){
		data = x;
		left = NULL;
		right = NULL;
	};
};

void preorder(Node *root){
	if(root == NULL)
		return;
	cout << root -> data << " ";
	preorder(root -> left);
	preorder(root -> right);
}

Node *convert(string str, int *cursor){
    Node *temp =new Node(str[*cursor]);
    cout << "cursor " << *cursor << endl;
    (*cursor)++;
    if(str[*cursor] == '?'){
        (*cursor)++;
        temp -> left = convert(str, cursor);
    }
    if(str[*cursor] == ':'){
        (*cursor)++;
        temp -> right = convert(str,  cursor);
    }
    return temp;
    
}

Node *convertExpression(string str,int i)
{
    return convert(str, &i);
}

int main(){
	int t ;
	cin >> t;
	while(t--){
		string str;
		cin >> str;
		Node *root =  convertExpression(str,0);
		preorder(root);
		cout << endl;
	}
	return 0;
}
