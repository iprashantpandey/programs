#include<iostream>
using namespace std;

void swap (int n,int first,int second,int *arr){
	int temp = arr[first];
	arr[first] = arr[second];
	arr[second]= temp;
}
void sort(int n,int *startArr , int *endArr){
	
	for(int i=0;i<n-1;i++){
		int temp= 0;
		int max = startArr[0];
		for(int j=0;j<n-i;j++){
		 	if(max <= startArr[j]){
				max = startArr[j];
			       temp = j;	
			}
		}
//		cout << " i " << i << " temp " << temp<< " n -i " << n-i-1 << endl;
		swap(n,temp,n-i-1,startArr);
		swap(n,temp,n-i-1,endArr);
	}
/*	for(int i=0;i<n;i++)
		cout << startArr[i] << " ";
	cout << endl;
	for(int i=0;i<n;i++)
		cout << endArr[i] << " ";
	cout << endl;*/
}

int maxActivity(int endTime,int cursor,int last,int n,int *startArr,int *endArr,bool flag){
//	cout << "end time " << endTime << " cursor " << cursor << " flag " << flag << endl;
	if(flag && last <= startArr[cursor])
		return 0;
	if(cursor == n)
		return 0;
	if(endTime <= startArr[cursor] ){
		int k1= 1+maxActivity(endArr[cursor],cursor+1,endArr[cursor],n,startArr,endArr,false);
//		cout << " k1 " <<k1 << endl; 
	       	int k2=	maxActivity(endTime,cursor+1,endArr[cursor],n,startArr,endArr,true);
//		cout << "k2 " << k2 << " max " << max(k1,k2);
		return max(k1,k2);
	}
	else
		return  maxActivity(endTime,cursor+1,last,n,startArr,endArr,flag);
}

int main(){
        int t;
        cin >> t;
        while(t--){
		int n;
		cin >> n;
		int startArr[n],endArr[n];
		for(int i=0;i<n;i++)
			cin >> startArr[i];
		for(int i=0;i<n;i++)
			cin >> endArr[i];
		sort(n,startArr,endArr);
		cout << maxActivity(0,0,endArr[0],n,startArr,endArr,false) << endl;
        }
        return 0;
}
