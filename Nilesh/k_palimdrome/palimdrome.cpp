#include<iostream>
#include<cstring>
using namespace std;

int dp[100][100];
int check(int i, int j, string s){
	//cout << " i " <<i << " j " << j << " k " <<k << endl;
    if(i >= j)
        return 0;
    if(dp[i][j] != -1)
	    return dp[i][j];
    if(s[i] != s[j])
        dp[i][j] =  min(check(i+1,j,s), check(i,j-1,s))+1;
    else
	    dp[i][j] = check(i+1,j-1,s);
    return dp[i][j];
}

bool is_k_palin(string s,int k)
{
	memset(dp,-1,sizeof(dp));
    int val= check(0,s.length()-1,s);
    if(val > k)
	    return 0;
    else 
	    return 1;
}

int main(){
	int t;
	cin >> t;
	while(t--){
		string s;
		cin >> s;
		int n;
		cin >> n;
		cout << is_k_palin(s,n) << endl;
	}
	return 0;
}
